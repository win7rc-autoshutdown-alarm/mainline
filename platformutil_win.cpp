#include "platformutil.h"


#define WIN32_LEAN_AND_MEAN
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "advapi32.lib")
#include <windows.h>

namespace nsWin7RCAutoShutdownAlarm {

long PlatformUtil::getMilliSecsSinceBoot()
{
    return ::GetTickCount();
}

//http://msdn.microsoft.com/en-us/library/aa376868(VS.85).aspx
//EWX_FORCEIFHUNG
bool PlatformUtil::shutdown()
{
//#ifdef Q_OS_WIN
   HANDLE hToken;
   TOKEN_PRIVILEGES tkp;

   // Get a token for this process.

   if (!OpenProcessToken(GetCurrentProcess(),
        TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
      return( FALSE );

   // Get the LUID for the shutdown privilege.

   LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME,
        &tkp.Privileges[0].Luid);

   tkp.PrivilegeCount = 1;  // one privilege to set
   tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

   // Get the shutdown privilege for this process.
   AdjustTokenPrivileges(hToken, FALSE, &tkp, 0,
        (PTOKEN_PRIVILEGES)NULL, 0);

   if (GetLastError() != ERROR_SUCCESS)
      return FALSE;

   // Shut down the system and force all applications to close.
//| EWX_FORCE
   if (!ExitWindowsEx(EWX_SHUTDOWN ,
               SHTDN_REASON_MAJOR_OPERATINGSYSTEM |
               SHTDN_REASON_MINOR_MAINTENANCE |
               SHTDN_REASON_FLAG_PLANNED))
      return FALSE;

   //shutdown was successful
   return TRUE;
}

} //namespace nsWin7RCAutoShutdownAlarm
