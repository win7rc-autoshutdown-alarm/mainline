#ifndef TIMERWIDGET_H
#define TIMERWIDGET_H

#include <QWidget>
#include <QPoint>
#include <QDateTime>

class QMenu;

namespace nsWin7RCAutoShutdownAlarm {

class TimerWidget : public QWidget {
    Q_OBJECT
public:
    TimerWidget(QDateTime bootTime, QMenu* contextMenu, QWidget *parent = 0);
    ~TimerWidget();

    static void paintPie(QPainter & painter, int size, float progress, bool systray = false);
    void setBootTime(QDateTime bootTime);
public slots:
    void update();

protected:
    void paintEvent(QPaintEvent *event);
    void closeEvent(QCloseEvent* event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void resizeEvent(QResizeEvent * event);
    void wheelEvent ( QWheelEvent * event );
    QSize sizeHint() const;

    void contextMenuEvent ( QContextMenuEvent * event );

private:
    float progress;
    QPoint dragPosition;
    QMenu* contextMenu;
    QDateTime bootTime;
};

} //namespace nsWin7RCAutoShutdownAlarm

#endif // TIMERWIDGET_H
