#include "countdownapp.h"
#include "ui_configdialog.h"
#include "configdialog.h"
#include "timerwidget.h"
#include "platformutil.h"

#include <QtWidgets>
#include <QtCore>

namespace nsWin7RCAutoShutdownAlarm {

CountdownApp::CountdownApp(QCoreApplication* parent)
    : QObject(parent), configDialog(0), timerWidget(0)
{
    config = new Config();
    app = parent;

    initTimers(config->timerOffset == "startup" ? 0 : PlatformUtil::getMilliSecsSinceBoot());
    initGUI();
}

CountdownApp::~CountdownApp()
{
    if (timerWidget) {
        config->pieShow = timerWidget->isVisible();
        timerWidget->close();
        delete timerWidget;
    }
    delete configDialog;
    delete config;
}

void CountdownApp::initGUI()
{
    systrayIcon = new QSystemTrayIcon(this);
    systrayIcon->setToolTip("Win7RC AutoShutdown Alarm");
    connect(systrayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(onSystrayIcon_clicked(QSystemTrayIcon::ActivationReason)));
    contextMenu = new QMenu();
    contextMenu->addAction("Toggle widget", this, SLOT(toggleTimerWidget()));
    contextMenu->addAction("Reset timer", this, SLOT(resetTimers()));
    contextMenu->addAction("Config..", this, SLOT(showConfigDialog()));
    contextMenu->addAction("Quit", app, SLOT(quit()));
    systrayIcon->setContextMenu(contextMenu);
    updateSystrayIcon();
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateSystrayIcon()));
    timer->start(config->pieUpdateFreq * 1000);


    systrayIcon->show();

    QRect geom = systrayIcon->geometry();
    qDebug() << "systray icon geom = " << geom;

    if (config->pieShow) {
        toggleTimerWidget();
    }
}

QTime qtimeFromSecs(int secs)
{
    if (secs == 0) return QTime();
    else return QTime().addSecs(secs);
}

void CountdownApp::initTimers(long millisSinceBoot) {
    QDateTime now = QDateTime::currentDateTime();

    qDebug() << "current time = " << now;
    bootTime = now.addMSecs(-millisSinceBoot);
    qDebug() << "adjusting for boot time =" << bootTime;
    ctimer1 = new CountdownTimer(bootTime.addSecs(config->timer1AfterSecs),
                       PlaySoundQt::CreateSound(QString(config->timer1SoundFile)),
                        qtimeFromSecs(config->timer1RepeatSecs),false,this);
    ctimer2 = new CountdownTimer(bootTime.addSecs(config->timer2AfterSecs),
                       PlaySoundQt::CreateSound(QString(config->timer2SoundFile)),
                        qtimeFromSecs(config->timer2RepeatSecs),false,this);
    endTime = bootTime.addSecs(config->timer2AfterSecs);
    qDebug() << "end time = " << endTime;
    shutdownTimer = new QTimer(this);
    shutdownTimer->setSingleShot(true);
    shutdownTimer->setInterval(QDateTime::currentDateTime().secsTo(bootTime.addSecs(config->timer3AfterSecs)) * 1000);
    connect(shutdownTimer, SIGNAL(timeout()), this, SLOT(shutdown()));
    shutdownTimer->start();
    qDebug() << "shutdown time = " << bootTime.addSecs(config->timer3AfterSecs);
}

void CountdownApp::resetTimers() {
    ctimer1->cancel();
    delete ctimer1;
    ctimer2->cancel();
    delete ctimer2;
    shutdownTimer->stop();
    delete shutdownTimer;
    initTimers(0);
    if (timerWidget != 0) {
        timerWidget->setBootTime(QDateTime::currentDateTime());
    }
}

void CountdownApp::test() {
//    qDebug() << "shutdown = " << PlatformUtil::shutdown();
//    qDebug() << "testing sound; available = " << QSound::isAvailable();
//    playSound("/home/mattie/Documents/test.wav");
}

void CountdownApp::toggleTimerWidget() {
    if (timerWidget == 0) {
        timerWidget = new TimerWidget(bootTime, contextMenu);
    }
//    qDebug() << timerWidget->isVisible();
    timerWidget->setVisible(!timerWidget->isVisible());
    config->pieShow = timerWidget->isVisible();
}

void CountdownApp::showConfigDialog()
{
    if (configDialog == 0) configDialog = new ConfigDialog(config);
    configDialog->loadConfig();
    connect(configDialog, SIGNAL(test()), this, SLOT(test()));
    configDialog->show();
}

void CountdownApp::onSystrayIcon_clicked(QSystemTrayIcon::ActivationReason reason)
{
    int secs = QDateTime::currentDateTime().secsTo(endTime);
    switch(reason) {
    case QSystemTrayIcon::Trigger:
        systrayIcon->showMessage("Win7RC shutdown reminder",
                                 QString("%1 hours %2 mins %3 secs left")
                                  .arg(secs/3600).arg((secs - (secs/3600)*3600)/60).arg(secs % 60),
                                 QSystemTrayIcon::Information, 1000);
        break;
    case QSystemTrayIcon::DoubleClick:
        toggleTimerWidget();
        break;
    case QSystemTrayIcon::Unknown:
    case QSystemTrayIcon::MiddleClick:
    case QSystemTrayIcon::Context:
    default:
        break;
    }
}

void CountdownApp::updateSystrayIcon()
{
    int progressSecs = bootTime.secsTo(QDateTime::currentDateTime());
    float progress = 1.f - ((float)progressSecs) / ((float)config->timer2AfterSecs);

    qreal iconsize = 16;
    QImage image(iconsize, iconsize, QImage::Format_ARGB32_Premultiplied);
    image.fill(0);
    QPainter painter(&image);
    painter.translate(iconsize / 2, iconsize / 2);
    TimerWidget::paintPie(painter, iconsize, progress, true);
    systrayIcon->setIcon(QIcon(QPixmap::fromImage(image)));
}

void CountdownApp::shutdown()
{
    qDebug() << "shutting down..";
    PlatformUtil::shutdown();
}

} //namespace nsWin7RCAutoShutdownAlarm
