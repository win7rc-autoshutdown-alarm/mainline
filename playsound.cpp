#include "playsound.h"

#include <QtGlobal>

#include <QSound>

namespace nsWin7RCAutoShutdownAlarm {

PlaySoundQt::PlaySoundQt(QString filename)
    : filename(filename)
{
}

void PlaySoundQt::play(bool loop)
{
    QSound sound(filename);
    if (loop) sound.setLoops(-1);
    sound.play();
}

PlaySoundQt* PlaySoundQt::CreateSound(QString filename)
{
    return new PlaySoundQt(filename);
}

} //namespace nsWin7RCAutoShutdownAlarm
