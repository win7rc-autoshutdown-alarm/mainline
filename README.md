Considering a home environment, the biggest advantage of having a windows install, is that games run out of the box. But who wants to pay 100-200 euros 'just for fun'? Windows 7 RC is the ideal free solution. There's only one catch: it reboots every 2 hours :) But as we all know, taking a break every 2 hours is quite healthy ;) So..

Win7 pops up a dialog way too early (after about 1 hour?), but then again, who sees that when you're playing. On top of that, windows does not even perform a proper shutdown, but really an instant power-off. 
When you're in the heat of a battle, you're not keeping an eye on the watch. That's where Win7RCAutoShutdownAlarm comes in.

Just have it in your autostart folder. It will notify you after 1h45 with a beep every minute. After 1h55, it will notify you every 2 secs, since shutdown is quite imminent.

wintendo rc 7 ftw! ;)
