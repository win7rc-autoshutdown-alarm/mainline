#ifndef PLAYSOUND_H
#define PLAYSOUND_H

#include <QtCore>

namespace nsWin7RCAutoShutdownAlarm {

class PlaySoundQt
{
public:
    PlaySoundQt(QString filename);
    ~PlaySoundQt();

    void play(bool loop = 0);

    static PlaySoundQt* CreateSound(QString filename);
protected:
    QString filename;
};

} //namespace nsWin7RCAutoShutdownAlarm

#endif // PLAYSOUND_H
