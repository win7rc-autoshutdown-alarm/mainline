#include "timerwidget.h"

#include "config.h"

#include <qpainter.h>
#include <QtDebug>
#include <QMouseEvent>
#include <QMenu>
#include <QTimer>

namespace nsWin7RCAutoShutdownAlarm {

TimerWidget::TimerWidget(QDateTime bootTime, QMenu* contextMenu, QWidget *parent) :
    QWidget(parent), bootTime(bootTime), contextMenu(contextMenu), progress(0.25f)
{
    setWindowTitle(tr("Time left"));
    resize(config->pieWinSize);
    move(config->pieWinPos);
    setToolTip("Drag to move\n"
               "Right-click for context menu\n"
               "Scroll to resize");
    setWindowIcon(QIcon(":/images/bell_256.png"));
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(config->pieUpdateFreq * 1000);
    update();
}

TimerWidget::~TimerWidget()
{
}

void TimerWidget::paintPie(QPainter & painter, int size, float progress, bool systray)
{
    float threshold = 1.f - (float)config->timer1AfterSecs / config->timer2AfterSecs;
    qDebug() << "threshold = " << threshold;
    QColor color(0, 255, 0);
    if (progress < threshold) {
        float ratio = progress / threshold;
        color = QColor::fromRgbF(1.f - ratio, ratio, 0);
    }
    QRect rect(-size/2,-size/2,size,size);
    QRadialGradient grad(0, 0, size / 2, 0, 0);
    grad.setColorAt(0, color.darker());
    grad.setColorAt(1, color);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setPen(Qt::NoPen);

    painter.setBrush(QColor(255,255,255,70));
    painter.drawEllipse(rect);

    painter.setBrush(QBrush(grad));
    painter.drawPie(rect, 16*90, progress*16*360);

    if (!systray) {
        painter.setPen(config->clockFontColor);
        painter.setFont(QFont(config->clockFontName, config->clockFontSize));
        QTime now = QTime::currentTime();
        painter.drawText(rect, Qt::AlignCenter, QString("%1:%2").arg(now.hour()).arg(now.minute(), 2, 10, (const QChar &)'0'));
    }
}

void TimerWidget::setBootTime(QDateTime bootTime)
{
    this->bootTime = bootTime;
    update();
}

void TimerWidget::closeEvent(QCloseEvent* event)
{
    qDebug() << "TimerWidget::closeEvent";
    config->pieWinSize = size();
    config->pieWinPos = pos();
    QWidget::closeEvent(event);
}

void TimerWidget::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        dragPosition = event->globalPos() - frameGeometry().topLeft();
        event->accept();
    }
}

void TimerWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton) {
        move(event->globalPos() - dragPosition);
        event->accept();
    }
}

void TimerWidget::resizeEvent(QResizeEvent * /* event */)
{
    int side = qMin(width(), height());
    QRegion maskedRegion(width() / 2 - side / 2, height() / 2 - side / 2, side,
                         side, QRegion::Ellipse);
    setMask(maskedRegion);
}

void TimerWidget::paintEvent(QPaintEvent *event)
{
    int side = qMin(width(), height());
    QPainter painter(this);
    painter.translate(width() / 2, height() / 2);
    paintPie(painter, side, progress);
}

#define sign(a) (a < 0 ? -1 : 1)

void TimerWidget::wheelEvent ( QWheelEvent * event )
{

    qDebug() << "TimerWidget::wheelEvent" << event->delta();
    QRect widgetSize(geometry());
    qDebug() << "rect = " << widgetSize;
    int side = (1.f + (.1f * sign(event->delta()))) * widgetSize.height();
    widgetSize.setHeight(side);
    widgetSize.setWidth(side);
    setGeometry(widgetSize);
    event->accept();
}

QSize TimerWidget::sizeHint() const
{
    return QSize(200, 200);
}

void TimerWidget::contextMenuEvent ( QContextMenuEvent * event )
{
    contextMenu->exec(event->globalPos());
}

void TimerWidget::update()
{
    int progressSecs = bootTime.secsTo(QDateTime::currentDateTime());
    this->progress = 1.f - ((float)progressSecs) / ((float)config->timer2AfterSecs);
    qDebug() << "TimerWidget::update()" << progressSecs << progress;
    repaint();
}

} //namespace nsWin7RCAutoShutdownAlarm
