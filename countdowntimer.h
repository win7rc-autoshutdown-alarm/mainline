#ifndef COUNTDOWNTIMER_H
#define COUNTDOWNTIMER_H

#include "playsound.h"

#include <QtCore>
#include <QtGui>

namespace nsWin7RCAutoShutdownAlarm {

class CountdownTimer : public QObject
{
Q_OBJECT
public:
    explicit CountdownTimer(QDateTime triggerTime, PlaySoundQt* sound, QTime freq, bool loop, QObject *parent = 0);

signals:

public slots:
    void zeroHour();
    void onFreq();
    void cancel();

private:
    QTimer timer, timerSS;
    PlaySoundQt* sound;
    QTime freq;
    bool loop;
};

} //namespace nsWin7RCAutoShutdownAlarm

#endif // COUNTDOWNTIMER_H
