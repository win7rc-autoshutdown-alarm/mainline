#ifndef PLATFORMUTIL_H
#define PLATFORMUTIL_H


namespace nsWin7RCAutoShutdownAlarm {

class PlatformUtil
{
public:
    static long getMilliSecsSinceBoot();
    static bool shutdown();
};

} // namespace nsWin7RCAutoShutdownAlarm

#endif // PLATFORMUTIL_H
