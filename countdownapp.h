#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "countdowntimer.h"
#include "ui_configdialog.h"
#include "config.h"

#include <QSystemTrayIcon>
#include <QMainWindow>
class QMenu;
class QTimer;

namespace nsWin7RCAutoShutdownAlarm {

class ConfigDialog;
class TimerWidget;

class CountdownApp : public QObject {
    Q_OBJECT
public:
    CountdownApp(QCoreApplication* parent = NULL);
    ~CountdownApp();

protected:
    void changeEvent(QEvent *e);

public slots:
    void test();
    void showConfigDialog();
    void toggleTimerWidget();
    void resetTimers();
    void onSystrayIcon_clicked(QSystemTrayIcon::ActivationReason);
    void updateSystrayIcon();
    void shutdown();

private:
    void initGUI();
    void initTimers(long millisSinceBoot);

    CountdownTimer *ctimer1, *ctimer2;
    QTimer* shutdownTimer;
    QDateTime bootTime;
    QDateTime endTime;
    ConfigDialog* configDialog;
    TimerWidget* timerWidget;
    QSystemTrayIcon* systrayIcon;
    QCoreApplication* app;
    QMenu* contextMenu;
};

} //namespace nsWin7RCAutoShutdownAlarm

#endif // MAINWINDOW_H
