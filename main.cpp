#include "countdownapp.h"

#include <QApplication>

using namespace nsWin7RCAutoShutdownAlarm;

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("Mattiesworld");
    QCoreApplication::setOrganizationDomain("mattiesworld.gotdns.org");
    QCoreApplication::setApplicationName("Win7RCAutoShutdownAlarm");

    QApplication app(argc, argv);
    app.setQuitOnLastWindowClosed(false);
    CountdownApp w(&app);
    return app.exec();
}
