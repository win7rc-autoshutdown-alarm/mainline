#ifndef CONFIG_H
#define CONFIG_H

#include <QString>
#include <QSize>
#include <QPoint>
#include <QColor>

namespace nsWin7RCAutoShutdownAlarm {

class Config
{
public:
    Config();
    ~Config();

    void load();
    void save();

    QString timerOffset;

    int timer1AfterSecs;
    int timer1RepeatSecs;
    QString timer1SoundFile;

    int timer2AfterSecs;
    int timer2RepeatSecs;
    QString timer2SoundFile;

    int timer3AfterSecs;
    QString timer3SoundFile;

    bool pieShow;
    QSize pieWinSize; //don't use save/restore geometry functions here.
    QPoint pieWinPos;
    int pieUpdateFreq;

    bool clockShow;
    QString clockFontName;
    QColor clockFontColor;
    int clockFontSize;
};

extern Config* config;

} //namespace nsWin7RCAutoShutdownAlarm

#endif // CONFIG_H
