#include "countdowntimer.h"

#include "playsound.h"

namespace nsWin7RCAutoShutdownAlarm {

CountdownTimer::CountdownTimer(QDateTime triggerTime, PlaySoundQt* sound, QTime freq, bool loop, QObject *parent)
    : QObject(parent), sound(sound), freq(freq), loop(loop)
{
    qDebug() << " trigger time = " << triggerTime;
    timerSS.setInterval(QDateTime::currentDateTime().secsTo(triggerTime) * 1000);
    timerSS.setSingleShot(true);
    connect(&timerSS, SIGNAL(timeout()), this, SLOT(zeroHour()));
    timerSS.start();
    if (!freq.isNull()) {
        timer.setInterval((freq.hour()*3600+freq.minute()*60+freq.second()) * 1000);
        qDebug() << "interval = " << timer.interval();
        connect(&timer, SIGNAL(timeout()), this, SLOT(onFreq()));
    }
}

void CountdownTimer::zeroHour()
{
    qDebug() << "0 hour";
    sound->play();
    if (!freq.isNull()) {
        timer.start();
    }
}

void CountdownTimer::onFreq()
{
    qDebug() << "onfreq";
    sound->play();
}

void CountdownTimer::cancel()
{
    timerSS.stop();
    timer.stop();
}

} //namespace nsWin7RCAutoShutdownAlarm
