#include "platformutil.h"

#include <fstream>

namespace nsWin7RCAutoShutdownAlarm {

long PlatformUtil::getMilliSecsSinceBoot()
{
    double uptime_seconds;
    if (std::ifstream("/proc/uptime", std::ios::in) >> uptime_seconds)
    {
        return static_cast<unsigned long long>(uptime_seconds)*1000ULL;
    } else return -1;

}

bool PlatformUtil::shutdown()
{
    //http://stackoverflow.com/questions/28812514/how-to-shutdown-linux-using-c-or-qt-without-call-to-system
    system("/bin/sh shutdown -P now");
    return true;
}

} //namespace nsWin7RCAutoShutdownAlarm
