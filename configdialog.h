#ifndef CONFIGDIALOG_H
#define CONFIGDIALOG_H

#include "config.h"
#include "ui_configdialog.h"
#include <QDialog>

namespace nsWin7RCAutoShutdownAlarm {

class ConfigDialog : public QDialog, public Ui::ConfigDialog
{
Q_OBJECT
public:
    explicit ConfigDialog(Config* config, QWidget *parent = 0);

signals:
    void test();

public:
    void loadConfig();

public slots:
    void onAccept();

    //autoconnect slots
    void on_timer1BrowseButton_clicked();
    void on_timer2BrowseButton_clicked();
    void on_timer3BrowseButton_clicked();
    void on_clockColorButton_clicked();

private:
    void initGui();
    void saveConfig();

    Config* config;
};

} //namespace nsWin7RCAutoShutdownAlarm

#endif // CONFIGDIALOG_H
