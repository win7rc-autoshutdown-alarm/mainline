# -------------------------------------------------
# Project created by QtCreator 2010-03-06T21:45:16
# -------------------------------------------------
TARGET = Win7RCAutoShutdownAlarm
QT += widgets multimedia
TEMPLATE = app
CONFIG -= console
RC_FILE = win7rc-autoshutdown-alarm.rc
# see http://doc.trolltech.com/4.6/qmake-function-reference.html#config-config
CONFIG(debug, debug|release) {
    message(Debug build)
    CONFIG += console
}
CONFIG(release, debug|release) {
    message(Release build)
}
SOURCES += main.cpp \
    countdowntimer.cpp \
    playsound.cpp \
    config.cpp \
    countdownapp.cpp \
    configdialog.cpp \
    timerwidget.cpp \
    platformutil_win.cpp \
    platformutil.cpp \
    platformutil_lin.cpp
HEADERS += countdowntimer.h \
    playsound.h \
    config.h \
    countdownapp.h \
    configdialog.h \
    timerwidget.h \
    platformutil.h
FORMS += configdialog.ui

RESOURCES += \
    win7rc-autoshutdown-alarm.qrc
