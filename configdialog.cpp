#include "configdialog.h"

#include <QtDebug>
#include <QFileDialog>
#include <QColorDialog>

namespace nsWin7RCAutoShutdownAlarm {

ConfigDialog::ConfigDialog(Config* config, QWidget *parent) :
    QDialog(parent), config(config)
{
    setupUi(this);
    initGui();

    connect(buttonBox, SIGNAL(accepted()), this, SLOT(onAccept()));

    connect(timer1PlayCB, SIGNAL(toggled(bool)), timer1SoundFileLE, SLOT(setEnabled(bool)));
    connect(timer2PlayCB, SIGNAL(toggled(bool)), timer2SoundFileLE, SLOT(setEnabled(bool)));
    connect(timer3PlayCB, SIGNAL(toggled(bool)), timer3SoundFileLE, SLOT(setEnabled(bool)));
    connect(testButton, SIGNAL(clicked()), this, SIGNAL(test()));
}

void ConfigDialog::initGui()
{
    QStringList timerOffsets;
    timerOffsets << "boot" << "startup";
    timerOffsetCB->insertItems(0, timerOffsets);
}

void ConfigDialog::onAccept()
{
    saveConfig();
}

void ConfigDialog::loadConfig()
{
    qDebug() << "ConfigDialog::loadConfig(): populating dialog with config settings";

    timerOffsetCB->setCurrentText(config->timerOffset);

    timer1AfterSP->setValue(config->timer1AfterSecs / 60);
    timer1FreqMinSP->setValue(config->timer1RepeatSecs / 60);
    timer1FreqSecSP->setValue(config->timer1RepeatSecs % 60);
    timer1SoundFileLE->setText(config->timer1SoundFile);
    timer1PlayCB->setChecked(!config->timer1SoundFile.isEmpty());
    timer1SoundFileLE->setEnabled(timer1PlayCB->isChecked());

    timer2AfterSP->setValue(config->timer2AfterSecs / 60);
    timer2FreqMinSP->setValue(config->timer2RepeatSecs / 60);
    timer2FreqSecSP->setValue(config->timer2RepeatSecs % 60);
    timer2SoundFileLE->setText(config->timer2SoundFile);
    timer2PlayCB->setChecked(!config->timer2SoundFile.isEmpty());
    timer2SoundFileLE->setEnabled(timer2PlayCB->isChecked());

    timer3AfterSP->setValue(config->timer3AfterSecs / 60);
    timer3SoundFileLE->setText(config->timer3SoundFile);
    timer3PlayCB->setChecked(!config->timer3SoundFile.isEmpty());
    timer3SoundFileLE->setEnabled(timer3PlayCB->isChecked());

    widgetShowClockCB->setChecked(config->clockShow);
}

void ConfigDialog::saveConfig()
{
    qDebug() << "ConfigDialog::saveConfig(): write back to config var";

    config->timerOffset = timerOffsetCB->currentText();

    config->timer1AfterSecs = timer1AfterSP->value() * 60;
    config->timer1RepeatSecs = timer1FreqMinSP->value() * 60 + timer1FreqSecSP->value();
    config->timer1SoundFile = timer1SoundFileLE->text();
    if (!timer1PlayCB->isChecked()) config->timer1SoundFile = "";

    config->timer2AfterSecs = timer2AfterSP->value() * 60;
    config->timer2RepeatSecs = timer2FreqMinSP->value() * 60 + timer2FreqSecSP->value();
    config->timer2SoundFile = timer2SoundFileLE->text();
    if (!timer2PlayCB->isChecked()) config->timer2SoundFile = "";

    config->timer3AfterSecs = timer3AfterSP->value() * 60;
    config->timer3SoundFile = timer3SoundFileLE->text();
    if (!timer3PlayCB->isChecked()) config->timer3SoundFile = "";

    config->clockShow = widgetShowClockCB->isChecked();
}

void ConfigDialog::on_timer1BrowseButton_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, "Select alarm sound");
    if (!filename.isNull()) timer1SoundFileLE->setText(filename);
}

void ConfigDialog::on_timer2BrowseButton_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, "Select alarm sound");
    if (!filename.isNull()) timer2SoundFileLE->setText(filename);
}

void ConfigDialog::on_timer3BrowseButton_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, "Select alarm sound");
    if (!filename.isNull()) timer3SoundFileLE->setText(filename);
}

void ConfigDialog::on_clockColorButton_clicked() {
    QColor newColor = QColorDialog::getColor(config->clockFontColor, this);
    if (!newColor.isValid()) return;
    config->clockFontColor = newColor;
}

} //namespace nsWin7RCAutoShutdownAlarm
