#include "config.h"

#include <QtCore>

namespace nsWin7RCAutoShutdownAlarm {

Config* config;

Config::Config()
{
    load();
}

Config::~Config()
{
    save();
}

void Config::load()
{
    qDebug() << "reading settings";
    QSettings settings;

    timerOffset = settings.value("general/timerOffset", "boot").toString();

    timer1AfterSecs = settings.value("timer1/afterSecs", 105 * 60).toInt(); //1h45
    timer1RepeatSecs = settings.value("timer1/repeatSecs", 60).toInt();
    timer1SoundFile = settings.value("timer1/soundFile", "ping.wav").toString();

    timer2AfterSecs = settings.value("timer2/afterSecs", 115 * 60).toInt(); //1h55
    timer2RepeatSecs = settings.value("timer2/repeatSecs", 2).toInt();
    timer2SoundFile = settings.value("timer2/soundFile", "pop.wav").toString();

    timer3AfterSecs = settings.value("timer3/afterSecs", 117 * 60).toInt(); //1h57
    timer3SoundFile = settings.value("timer3/soundFile", "pop.wav").toString();

    pieShow = settings.value("pie/show", true).toBool();
    pieWinSize = settings.value("pie/size", QSize(200, 200)).toSize();
    pieWinPos = settings.value("pie/pos", QPoint(200, 200)).toPoint();
    pieUpdateFreq = settings.value("pie/updateFreq", 60).toInt();

    clockShow = settings.value("clock/show", true).toBool();
    clockFontName = settings.value("clock/fontName", "Arial").toString();
    clockFontColor = settings.value("clock/fontColor", QColor(255,255,255)).value<QColor>();
    clockFontSize = settings.value("clock/fontSize", 16).toInt();
}

void Config::save()
{
    qDebug() << "writing settings";
    QSettings settings;

    settings.setValue("general/timerOffset", timerOffset);

    settings.setValue("timer1/afterSecs", timer1AfterSecs);
    settings.setValue("timer1/repeatSecs", timer1RepeatSecs);
    settings.setValue("timer1/soundFile", timer1SoundFile);

    settings.setValue("timer2/afterSecs", timer2AfterSecs);
    settings.setValue("timer2/repeatSecs", timer2RepeatSecs);
    settings.setValue("timer2/soundFile", timer2SoundFile);

    settings.setValue("timer3/afterSecs", timer3AfterSecs);
    settings.setValue("timer3/soundFile", timer3SoundFile);

    settings.setValue("pie/show", pieShow);
    settings.setValue("pie/size", pieWinSize);
    settings.setValue("pie/pos", pieWinPos);
    settings.setValue("pie/updateFreq", pieUpdateFreq);

    settings.setValue("clock/show", clockShow);
    settings.setValue("clock/fontName", clockFontName);
    settings.setValue("clock/fontColor", clockFontColor);
    settings.setValue("clock/fontSize", clockFontSize);
}

} //namespace nsWin7RCAutoShutdownAlarm
